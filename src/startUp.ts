import { Player, BOARD } from './main';
import { WEAPONS } from './items';
import { getLoadedUser } from './saveLoad';

const REGEX: RegExp = new RegExp('[a-zA-Z0-9]', 'g');

const DOM: {
  BTN_START: HTMLButtonElement;
  BTN_SAVE: HTMLButtonElement;
  BTN_LOAD: HTMLButtonElement;
  INPUT: HTMLInputElement;
  USER: HTMLElement;
  GAME: HTMLElement;
  ERROR: HTMLElement;
} = {
  BTN_START: document.querySelector('.btn--start'),
  BTN_SAVE: document.querySelector('.btn--save'),
  BTN_LOAD: document.querySelector('.btn--load'),
  INPUT: document.querySelector('.user__input'),
  USER: document.querySelector('.user'),
  GAME: document.querySelector('.game'),
  ERROR: document.querySelector('.user__error')
};
DOM.BTN_SAVE.disabled = true;

let user: Player;

const inputToGame = (user: Player): void => {
  // Switch from user input to game
  DOM.USER.style.display = 'none';
  DOM.GAME.style.display = 'grid';

  // Enable save game button, disable load button
  DOM.BTN_SAVE.disabled = false;
  DOM.BTN_LOAD.disabled = true;

  // Player introduction
  BOARD.clear();
  BOARD.log(`You are ${user.name}, a ...`);
};

DOM.BTN_START.addEventListener('click', () => {
  /*
    Match username with RegExp for alphanumeric strings, create an array from the
    matches and use the length of the string and array to test, if the string
    includes unwanted characters
  */
  // @ts-ignore
  let temp: string[] = Array.from(DOM.INPUT.value.matchAll(REGEX), m => m[0]);

  // Username validation
  if (DOM.INPUT.value.length < 3 || DOM.INPUT.value.length !== temp.length)
    DOM.ERROR.innerHTML = 'Username must be 3+ alphanumeric characters';
  else {
    user = getLoadedUser() || new Player();

    user.setName(DOM.INPUT.value);
    user.setWeapon(WEAPONS.woodenStick);

    inputToGame(user);
  }
});

export { inputToGame, user };
