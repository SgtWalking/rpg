import { Weapon, ArmorPiece, Player } from './main';

const ENEMIES: {
  goblin: Player;
} = {
  goblin: new Player('Goblin', new Weapon('Skirmisher', 2), {
    helmet: 'None',
    harness: 'None',
    leggings: new ArmorPiece('Leather Shorts', 'Leggings', 2),
    boots: 'None'
  })
};

export { ENEMIES };
