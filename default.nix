{ pkgs ? import <nixpkgs> {  } }:

let
  inherit (pkgs) stdenv nodejs;
in stdenv.mkDerivation {
  name = "env";
  buildInputs = [ nodejs ];

  shellHook = ''
    export PATH="$PWD/node_modules/.bin/:$PATH"
    npm i typescript browserify uglify-js node-sass

    alias bundle="/bin/sh $PWD/bundle.sh"
  '';
}
