class Board {
  DOM: HTMLElement;

  constructor(DOM: HTMLElement) {
    this.DOM = DOM;
  }

  /**
   * Clear the board of text
   */
  clear = () => {
    while (this.DOM.hasChildNodes()) this.DOM.removeChild(this.DOM.lastChild);
  };

  /**
   * Print text to the board
   * @param {string} msg Text to be printed
   */
  private _log = (msg: string) =>
    this.DOM.insertAdjacentHTML(
      'beforeend',
      `${this.DOM.innerHTML !== '' ? '<br />' : ''}${msg}`
    );

  /**
   * Check if input is string or string[] and pass them on accordingly
   * @param {string | string[]} strs String(s) to be checked
   */
  log = (strs: string | string[]) =>
    typeof strs === 'string'
      ? this._log(strs)
      : strs.forEach(str => this._log(str));
}

export { Board };
