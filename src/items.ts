import { Weapon, ArmorPiece } from './main';

/*
  Armor
*/
const ARMOR: {
  leatherHelmet: ArmorPiece;
} = {
  leatherHelmet: new ArmorPiece('Leather Helmet', 'Helmet')
};

/*
  Weapons
*/
const WEAPONS: {
  woodenStick: Weapon;
} = {
  woodenStick: new Weapon('Wooden Stick', 5)
};

export { ARMOR, WEAPONS };
