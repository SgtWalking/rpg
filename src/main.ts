import { Board } from './board';

/*
  Constants
*/
const HIT_CHANCE: number = 0.8;
const BOARD: Board = new Board(document.querySelector('.game__text'));

/*
  Global functions
*/
const addZero = (n: number) => `00${n}`.slice(-3);

/*
  Item classes and types
*/
type ArmorPlayer = {
  helmet: ArmorPiece | 'None';
  harness: ArmorPiece | 'None';
  leggings: ArmorPiece | 'None';
  boots: ArmorPiece | 'None';
};
type ArmorType = 'Helmet' | 'Harness' | 'Leggings' | 'Boots';
type Modifier = 'None' | 'Fire' | 'Ice' | 'Lightning';
type BattleChoice = 'Fight' | 'Magic' | 'Item' | 'Run';

class ArmorPiece {
  name: string = 'Default';
  defense: number = 1;
  type: ArmorType;

  constructor(name: string, type: ArmorType, defense?: number) {
    this.name = name;
    this.type = type;
    if (defense) this.defense = defense;
  }
}

class Weapon {
  name: string = 'Default';
  attack: number = 1;
  modifier: Modifier = 'None';

  constructor(name: string, attack: number, modifier?: Modifier) {
    this.name = name;
    this.attack = attack;
    if (modifier) this.modifier = modifier;
  }
}

class Player {
  public name: string = 'Default';
  public displayName: string;
  private health: number = 100;
  private attack: number = 1;
  private defense: number = 0;
  private critChance: number = 0.3;
  private critMultiplier: number = 2;
  private weapon: Weapon;
  private armor: ArmorPlayer = {
    helmet: 'None',
    harness: 'None',
    leggings: 'None',
    boots: 'None'
  };
  dead: boolean = false;
  stats: { enemiesKilled: number } = {
    enemiesKilled: 0
  };

  constructor(name?: string, weapon?: Weapon, armor?: ArmorPlayer) {
    if (name) this.setName(name);
    if (weapon) this.setWeapon(weapon);
    if (armor) this.setArmor(armor);
  }

  // Setters
  /**
   * Set player name and display name
   * @param name Player name to be used
   */
  setName = (name: string) => {
    this.name = name;
    this.displayName = `[${name}]`;
  };

  /**
   * Set player weapon and calculate attack from it
   * @param weapon Weapon to be used
   */
  setWeapon = (weapon: Weapon) => {
    this.weapon = weapon;
    this.attack += weapon.attack;
  };

  /**
   * Set player armor and calculate defense from it
   * @param armor Set of armor to be used
   */
  setArmor = (armor: ArmorPlayer) => {
    this.armor = armor;

    // Calculate armor from armor pieces
    let temp: number = 0;
    // @ts-ignore
    Object.entries(this.armor).forEach(([key, value]) => {
      if (value !== 'None') temp += value.defense;
    });
    this.defense = temp;
  };

  // Getters
  /**
   * @returns {{ name: string, display: string }} Player name and display name
   */
  getName = (): { name: string; display: string } => {
    return { name: this.name, display: this.displayName };
  };
  /**
   * @returns {Weapon} Player weapon
   */
  getWeapon = (): Weapon => this.weapon;
  /**
   * @returns {ArmorPlayer} Player armor
   */
  getArmor = (): ArmorPlayer => this.armor;

  // Functions
  /**
   * Basic battle implementation
   * @param enemy Player to battle against
   */
  battle = (enemy: Player) => {
    let crit: boolean = Math.random() <= this.critChance;
    // Check if hit is `crit` and calculate new `attack` accordingly
    let attack: number = crit
      ? Math.round(this.attack * this.critMultiplier)
      : this.attack;

    if (!enemy.dead)
      Math.random() < HIT_CHANCE
        ? ((enemy.health -= attack - enemy.defense),
          BOARD.log(
            `${enemy.displayName} -${addZero(attack)}: ` +
              addZero(enemy.health)
          ))
        : BOARD.log(`${enemy.displayName} blocked`);
    else {
      BOARD.log("Can't battle this enemy");
      return;
    }

    if (enemy.health <= 0) {
      enemy.dead = true;
      BOARD.log(`${enemy.displayName} died`);
      this.stats.enemiesKilled++;
    }
  };
}

export { Weapon, ArmorPiece, ArmorPlayer, ArmorType, Player, BOARD };
