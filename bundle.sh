#!/bin/sh
# compile and bundle .ts and .scss

# .ts
cd src
ts=$(find . -name "*.ts")
[ "$ts" ] && tsc $ts || echo 'No .ts files'

js=$(find . -name "*.js" | grep -v 'bundle.js')
[ "$js" ] && browserify $js | uglifyjs > bundle.js || echo 'No .js files'
cd ..

# .scss
node-sass --output-style compressed style/bundle.scss > style/bundle.css
