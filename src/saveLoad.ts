import { inputToGame, user } from './startUp';
import { Player } from './main';
import { Base64 } from './base64';
const base64 = new Base64();

const DOM: {
  BTN_SAVE: HTMLButtonElement;
  BTN_LOAD: HTMLButtonElement;
} = {
  BTN_SAVE: document.querySelector('.btn--save'),
  BTN_LOAD: document.querySelector('.btn--load')
};

let loadedUser: Player = null;

/* const shuffledArrToString = (arr: any[]): string => {
  for (let i: number = arr.length - 1; i > 0; i--) {
    let j: number = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr.join('');
}; */

/**
 * Save current player info as base64 encoded string in a .txt file
 * @param {string} content Content to be put in the file
 */
const saveGame = (content: string) => {
  // Save first 5 numbers from `Date.now()`, randomize rest
  /* let stamp: string = Date.now().toString();
  let stamps: { start: string; end: string } = {
    start: stamp.slice(0, 5),
    end: stamp.slice(-8)
  };
  let saveName: string = `${stamps.start}${shuffledArrToString(
    stamps.end.split('')
  )}`; */

  let el: HTMLElement = document.createElement('a');
  el.setAttribute(
    'href',
    `data:text/json;charset=utf-8,${encodeURIComponent(content)}`
  );
  el.setAttribute('download', `saveGame_${Date.now()}.txt`);

  el.style.display = 'none';
  document.body.appendChild(el);
  el.click();
  document.body.removeChild(el);
};
DOM.BTN_SAVE.addEventListener('click', e =>
  saveGame(base64.encode(JSON.stringify(user ? user : loadedUser)))
);

/**
 * Load a file and read the content back into the `Player` object
 * @param {Event} e Default Event param – needed for file picker
 */
const loadGame = (e: Event) => {
  // @ts-ignore
  let file: File = e.target.files[0];

  // Get file data
  let reader: FileReader = new FileReader();
  reader.onload = () => {
    let data: string = reader.result as string;

    // Set Player object
    let json: Player = JSON.parse(base64.decode(data));
    let res: boolean = confirm(
      `Are you sure you want to load ${json.displayName}?`
    );

    if (res) {
      loadedUser = json;
      inputToGame(loadedUser);
    }
  };
  reader.readAsText(file);
};
DOM.BTN_LOAD.addEventListener('change', loadGame, false);

const getLoadedUser = () => loadedUser;
export { getLoadedUser };
